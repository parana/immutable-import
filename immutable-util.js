// Typescript não aceita URLs! <reference path='https://raw.githubusercontent.com/facebook/immutable-js/master/dist/immutable.d.ts'/>
// Polyserve não manipula isso ! <reference path='.../immutable/dist/immutable.d.ts'/>
///<reference path='bower_components/immutable/dist/immutable.d.ts'/>
//  Funções utilitárias associadas a API immutable.
var ensureImmutableIsReady = function (callback) {
    var immutableNotReady = function () {
        if ('undefined' != (typeof Immutable) &&
            'undefined' != (typeof Immutable.Map)) {
            return false;
        }
        return true;
    };
    var count = 0;
    var delaySubscribe = function () {
        if (immutableNotReady()) {
            // verifica num intervalo de 10 milisegundos
            setTimeout(delaySubscribe, 10);
            count++;
        }
        else {
            console.log('Immutable is ready in ' + (count * 10) + ' milliseconds.');
            setTimeout(callback, 50);
        }
    };
    delaySubscribe();
};
ensureImmutableIsReady(function () {
    console.log('••• Immutable is Ready now');
});
