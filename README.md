# immutable-import

Este WebComponent integra https://github.com/facebook/immutable-js como
elemento ao Polymer. 

Nesta versão integra :

* dist/immutable.d.ts
* dist/immutable.min.js

Veja esta discução interessante sobre Imutabilidade :

[http://www.yegor256.com/2014/12/09/immutable-object-state-and-behavior.html](http://www.yegor256.com/2014/12/09/immutable-object-state-and-behavior.html) 

Veja também o site da do projeto [Immutable JS](http://facebook.github.io/immutable-js/docs/#/List/filter)

